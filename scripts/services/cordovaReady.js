'use strict';

angular.module('KomunikatorAngularYeomanApp')
  .factory('cordovaReady',  function ($rootScope) {
    return function (fn) {
      var queue = [];

      var impl = function () {
        queue.push(Array.prototype.slice.call(arguments));
      };

      document.addEventListener('deviceready', function () {
        console.log(" devajs je ready");
        queue.forEach(function (args) {
          fn.apply(this, args);
        });
        impl = fn;
      }, false);
      
      return function () {
        return impl.apply(this, arguments);
      };
    };
  });