'use strict';

KomunikatorAngularYeomanApp.factory('audioManager', function($rootScope, cordovaReady) {
  // Service logic
  // ...
 function playAudioList(list) {

      	var audio = [];
		// Array of files you'd like played
		audio.playlist = list;
		 
		function playAudio(playlistId) {
		    // Default playlistId to 0 if not supplied 
		  //  playlistId = playlistId ? playlistId : 0;
		    if(playlistId < audio.playlist.length){
			    var firstMedia = new  Audio5js({
			      swf_path: 'components/audio5js/audio5js.swf',
			      throw_errors: true,
			      format_time: true,
			      ready: function (player) {
			        //this points to the audio5js instance
			        this.load(audio.playlist[playlistId].zvok);
			        this.play();
			        console.log("začel  zvokom");
			        console.log(audio.playlist[playlistId].zvok);
			        //will output {engine:'html', codec: 'mp3'} in browsers that support MP3 playback.
			        // will output {engine:'flash', codec: 'mp3'} otherwise	        
			        console.log(player);
			        this.on('ended', function(){
			                playlistId ++;
			                playAudio(playlistId);
			            }, this);
			      }
			    });
		 	}  
		}
		 
		// Start
		playAudio(0);
      
      }

       var nextMedia = null;
      var firstMedia = null;
      
      function playAudioListCordova(list) {
      	var listIndex = 1;
        var mediaList = list;
        if (firstMedia!= null) {
        	firstMedia.release();
        	console.log("izpustil sem firstMedia");
        }
      	firstMedia = new Media(mediaList[0].zvok, playSuccess, onError);
		console.log("začel 1 zvokom");
		firstMedia.play();		 

		function playSuccess() {			
		 	if (listIndex < mediaList.length) {
		 	    listIndex+=1; 
		 	    if (nextMedia!= null) {
		 	    	nextMedia.release();
		 	    	console.log("izpustil sem nextMedia");
		 	    }
		 		console.log("začel z  zvokom", listIndex);
		 		nextMedia = new Media(mediaList[listIndex-1].zvok,  playSuccess, onError);
		 		nextMedia.play();
						 		
		 	} 
		 
		 }	
      
      }

        // onError Callback 
        //
        function onError(error) {
          
            alert('code: '    + error.code    + '\n' + 
                  'message: ' + error.message + '\n');
        }

 

  // Public API here
  return {
   playAudioListwww: playAudioList,
   getCurrentPosition: cordovaReady(function (onSuccess, onError, options) {
      navigator.geolocation.getCurrentPosition(function () {
        var that = this,
          args = arguments;
          
        if (onSuccess) {
          $rootScope.$apply(function () {
            onSuccess.apply(that, args);
          });
        }
      }, function () {
        var that = this,
          args = arguments;
          
        if (onError) {
          $rootScope.$apply(function () {
            onError.apply(that, args);
          });
        }
      },
      options);
    })
   ,
   playAudioListCordova: cordovaReady(function (list) {
         playAudioListCordova(list);
    	})
  };
});
