'use strict';

angular.module('KomunikatorAngularYeomanApp')
  .factory('persistantStorage', function ($rootScope, cordovaReady, $q) {


    function getCordovaFile (storage) {
      var parsedStorage = {};
      var deferred = $q.defer();

      //cordovaReady(function(storageID) {
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, gotError); 
      // console.log("parsedStorage");
      //  console.log(angular.fromJson(parsedStorage));
    //});

      //return parsedStorage;

      //File API read file
       function gotFS(fileSystem) {
         console.log("imam file System 2");
          fileSystem.root.getFile("lokalniSlovar.txt", null, gotFileEntry, gotError);
      }

      function gotFileEntry(fileEntry) {
        console.log("imam fileEntry");
          fileEntry.file(gotFile, gotError);
      }

      function gotFile(file){
          //readDataUrl(file);
          console.log("imam datoteko in jo bom poskusil prebrati");
          readAsText(file);
      }

      // function readDataUrl(file) {
      //     var reader = new FileReader();
      //     reader.onloadend = function(evt) {
      //         console.log("Read as data URL");
      //         console.log(evt.target.result);
      //     };
      //     reader.readAsDataURL(file);
      // }

      function readAsText(file) {
          var reader = new FileReader();
          reader.onloadend = function(evt) {
              console.log("Read as text");
              console.log(evt.target.result);
              deferred.resolve(evt.target.result);
                    $rootScope.$apply();

             
          };
          reader.readAsText(file);
      }

      function gotError(evt) {
        console.log("error objekt");
        console.log(evt);
        deferred.reject(evt);
        $rootScope.$apply();
      }

      return deferred.promise;
    }

  function putCordovaFile(item, storage) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, gotError); 

      //File API read file
       function gotFS(fileSystem) {
        console.log("imam file System");
          fileSystem.root.getFile("lokalniSlovar.txt", {create: true, exclusive: false}, gotFileEntry, gotError);
      }

      function gotFileEntry(fileEntry) {
        console.log("imam fileEntry");
            
          fileEntry.createWriter(gotFileWriter, gotError);
      }

      function gotFileWriter(writer) {
         writer.onwriteend = function(evt) {
            console.log("contents of file now Item");
            
        };
        writer.write(angular.toJson(item));
    }
  }

  function gotError(evt) {
        console.log("error objekt");
        console.log(evt);
    }

  return {
    getwww: function(storageID) {
      console.log("berem localStorage");
      $rootScope.favoritesList = JSON.parse(localStorage.getItem(storageID) || '[]');

     // return JSON.parse(localStorage.getItem(storageID) || '[]');
    },

    putwww: function(item , storageID ) {
      localStorage.setItem(storageID, JSON.stringify(item));
    },

    getCordova:  getCordovaFile, 
    // function(){
      // var defer = $q.defer();
      // alert(" persistantStorage getCordova called");
      //cordovaReady(function(storageID ) {
    //   var defer = $q.defer();
    //   // getCordovaFile(storageID).then(function(data) {
    //     alert("promise je bil izvršen")
    //     defer.resolve();
    //     $rootScope.$apply();
    //   //   //return data
    //    //}); 
    //   // });
    //    return defer.promise;
    // } ,

    putCordova: //cordovaReady(
      function( todos, storageID ) {
      console.log(" persistantStorage putCordova called");
      putCordovaFile(todos, storageID);
      //console.log(" persistantStorage putCordova");
    }//)
  };
});