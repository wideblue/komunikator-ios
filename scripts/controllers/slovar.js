'use strict';

KomunikatorAngularYeomanApp.controller('SlovarCtrl', function($scope, $routeParams, $http, audioManager) {
 	
	function izvleciPodvnos(objektvnos, stringPoti ){    
		var arejPoti = stringPoti.split('_');
		var vmesniObjekt = objektvnos;
		for (var i = 1 ; i < arejPoti.length; i++) {
			vmesniObjekt=vmesniObjekt.vnos[arejPoti[i]]
			}
	  return    vmesniObjekt
	}

	var podvnos = {}; 
	
	// $http.get('slovar.json').success(function(data) {
		 // console.log("data");
         // console.log(data);
		
		
	// var slovarJSON = angular.copy(data);	
	
	 console.log("slovarJSON");
     console.log($scope.slovarJSON());
	  	  
	podvnos = izvleciPodvnos($scope.slovarJSON()[0], $routeParams.stran);
    console.log("podvnos");
    console.log(podvnos);
   
   angular.forEach(podvnos.vnos , function(podpodvnos, key){
		if (podpodvnos.vnos.length > 0) {
			podpodvnos.href = $routeParams.stran + "_"+key;
			console.log("podpodvnos.href ", podpodvnos.href);
			console.log(podpodvnos);
		} else {
			podpodvnos.href = $routeParams.stran;
		}
   });
      
	
    $scope.mainImageUrl = podvnos.slika;
    $scope.phones = podvnos.vnos;
	// });
	
	  // Called when capture operation is finished
    //
     var recordAudioBtn = document.getElementById("recordAudioBtn");
    function captureSuccess(mediaFiles) {
        // var i, len;
        // for (i = 0, len = mediaFiles.length; i < len; i += 1) {
        //   console.log("Pot do posnetka je " + mediaFiles[i].fullPath);
          
        //   recordAudioBtn.value = mediaFiles[i].fullPath;
       // }       
    }

    // Called if something bad happens.
    // 
    function captureError(error) {
        // var msg = 'An error occurred during capture: ' + error.code;
        // navigator.notification.alert(msg, null, 'Uh oh!');
    }

    // A button will call this function
    //
    
    $scope.recordAudio = function(){
        // // Launch device audio recording application, 
        // // allowing user to capture up to 1 audio clips
        // navigator.device.capture.captureAudio(captureSuccess, captureError, {limit: 1 , mode:"audio/amr"  });
    }
	
	$scope.deliteList = function() {
		// $scope.itemsList.splice(0,$scope.itemsList.length); // empty whole list
		 $scope.itemsList.splice(-1,1); // remove last element
	}
	
	$scope.playList = function(listToPlay) {
		audioManager["playAudioList"+$scope.platformIndicator](listToPlay);		
	}
	
	$scope.setImage = function(item) {
		$scope.mainImageUrl = item.slika;	
		$scope.itemsList.push(item);
		// var partsRecordName = item.zvok.split('.');
		// if (partsRecordName[1]=="3gpp"){
		// 	playAudio(item.zvok);
		// } else {
		// playAudio("/android_asset/www/"+ item.zvok);		
		audioManager["playAudioList"+$scope.platformIndicator]([item]);	 
		//  }
	}
	
	//add current itemsList to Favorits
	$scope.addToFavorites = function(item) {
		var newfavoritItem = angular.copy(item);
		$scope.favoritesList.push(newfavoritItem);
		console.log("dodal nov item to favorites");
		console.log(item);	 
	}
	
	$scope.removeFavorit = function(item) {
		$scope.favoritesList.splice($scope.favoritesList.indexOf(item), 1);
	}
	
	$scope.add = function (novVnos) {
	// var arejPoti = recordAudioBtn.value.split('/');
	// 	novVnos.zvok = arejPoti[arejPoti.length-1];
	// 	novVnos.slika = "";
	// 	novVnos.id	= "";
	// 	novVnos.datumvnosa = "";
	// 	novVnos.vnos	= [];
	
	// 	podvnos.vnos.push(angular.copy(novVnos));
	// 	shraniSlovarj();
	}

	$scope.doTheBack = function() {
	  window.history.back();
	};
 
	$scope.tabBarShow = true; 
	
	$scope.modalOpen = function (modalName) {
    $scope[modalName] = true;
    $scope.tabBarShow = false;
  };

  	$scope.modalClose = function (modalName) {
    $scope[modalName] = false;
     $scope.tabBarShow = true;
  };

  $scope.opts = {
    backdropFade: true,
    dialogFade:true
  };


  $scope.hrefAdd = $routeParams.stran;
  $scope.orderProp = 'age';
	// File API
   function shraniSlovarj() {		
        // window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);		
    }

    function gotFS(fileSystem) {
        fileSystem.root.getFile("lokalniSlovar.txt", {create: true, exclusive: false}, gotFileEntry, fail);
    }

    function gotFileEntry(fileEntry) {
        fileEntry.createWriter(gotFileWriter, fail);
    }

    function gotFileWriter(writer) {
        writer.onwriteend = function(evt) {
            console.log("contents of file now $scope.slovarJSON()[0]");
            
        };
        writer.write(angular.fromJson($scope.slovarJSON()));
    }

    function fail(error) {
        console.log(error);
    }


//PhoneDetailCtrl.$inject = ['$scope', '$routeParams', '$http'];


});
