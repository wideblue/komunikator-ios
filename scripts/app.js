'use strict';

var KomunikatorAngularYeomanApp = angular.module('KomunikatorAngularYeomanApp', ['ui.bootstrap'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/slovar/:stran', {
        templateUrl: 'views/slovar.html',
        controller: 'SlovarCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);

KomunikatorAngularYeomanApp.run(function($rootScope, $q, cordovaReady, persistantStorage ) {

  //string indicating  chosen platform   "www", "Cordova"
  $rootScope.platformIndicator = "Cordova";

 
 // function getSlovar(slovarUrl, $http ) {
    // $http.get(slovarUrl).success(function(data) {
      // slovarJSON = angular.copy(data);
    // });
  // }

  //var slovarJSON = angular.copy(slovar);
  // getSlovar('slovar.json');
  // getSlovar.$inject = ['$scope', '$http'];
  var PERSISTANT_STORAGE_FAVORITES_LIST_ID = 'KomunikatorFavoritesList';
      
  //$rootScope.favoritesList = [];
  // cordovaReady(function( ) {
     document.addEventListener("deviceready", onDeviceReady, false);

   //document.addEventListener('deviceready', 
    function onDeviceReady() {
    console.log("2DEVAJS READY")
  persistantStorage["get"+$rootScope.platformIndicator](PERSISTANT_STORAGE_FAVORITES_LIST_ID).then(function(data) {
    $rootScope.favoritesList = angular.fromJson(data);
     //alert("promis resolved");

     $rootScope.$watch(function() { return $rootScope.favoritesList; }, function() {
                console.log("shranil bom favoritesList");  
                persistantStorage.putCordova($rootScope.favoritesList, "lokalniSlovar.txt");  
              }, true);
  },
   function(reason){
     alert("promis rejected"); 
     $rootScope.favoritesList = []; 
     $rootScope.$watch(function() { return $rootScope.favoritesList; }, function() {
                console.log("shranil bom favoritesList");  
                persistantStorage.putCordova($rootScope.favoritesList, "lokalniSlovar.txt");  
              }, true);

   });
 }//);

    //|| [];
  //if favoritesList changed we write it in the persistant storage. In the case of Cordova platform
  // I attache $watch after $rootScope.favoritesList was read from storage. This part of the code is inside 
  // callback readAsText in persistanStorage 
 // if ($rootScope.platformIndicator == "www"){
 //  $rootScope.$watch(function() { return $rootScope.favoritesList; }, function() {
 //                     console.log("favorites so se spremenili klic persistan storage");
 //     persistantStorage["put"+$rootScope.platformIndicator]($rootScope.favoritesList, "lokalniSlovar.txt");
 //  }, true);
 // //}

  

  //$rootScope.favoritesList = [];
  $rootScope.itemsList = [];
  $rootScope.slovarJSON = function () { 
    
    return  slovar3;       //slovarJSON ;
  }
});